RUN TEST: 
On Windows: 
1. download project using link https://gitlab.com/VeraKaz/qatest
2. extract progect files in the directory
3. open terminal, go to the root project folder(in this case this is 'qatest')
4. run the command 'gradlew clean test'
5. wait a few minutes, after test executing you can see the link on the test report http://take.ms/9R7Vf

On MAC:
Steps the sameas steps for Windows but the command is different.
The command is 'gradle clean test'.

On Ubuntu:
Jar files and class files should be in current directory)
The command examples: 
1. javac -cp junit-4.11.jar:. TestSuit.java
2. java -cp junit-4.11.jar:hamcrest-core-1.3.jar:. org.junit.runner.JUnitCore TestSuit
