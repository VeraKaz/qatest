package auth;
import io.restassured.response.Response;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.Assert.assertEquals;
//import static api.ProtocolResponse.*;
import static api.GetCode.*;


@RunWith(JUnitParamsRunner.class)
public class UserApplicationLoginTest {
    private static Logger logger = LogManager.getLogger();
    private static int expectedStatusCode = 200;
    private static Response response = null;

    @Test
    public void testApplicationLogin() {
        // web application
        String code = getCode(Codes.WEB);
        response = given().
                param("code", code).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("WEB app, status code 200: " + response.asString());
        response.then().statusCode(expectedStatusCode); // 200

        // android application
        code = getCode(Codes.ANDOID);
        response = given().
                param("code", code).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Android app, status code 200: " + response.asString());
        response.then().statusCode(expectedStatusCode); // 200

        //ios application
        code = getCode(Codes.IOS);
        response = given().
                param("code", code).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("IOS app, status code 200: " + response.asString());
        response.then().statusCode(expectedStatusCode); // 200

        //wrong code
        response = given().
                param("code", email).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Wrong authorize code, status code 401: unauthorized " + response.asString());
        response.then().statusCode(401); // 401

        //wrong user email
        response = given().
                param("code", code).
                param("email", "vera.kazimirova@uadevelopers.c").
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Wrong 'email', status code 404: not found " + response.asString());
        response.then().statusCode(404); // 404

        //wrong password
        response = given().
                param("code", code).
                param("email", email).
                param("password", "46c5c023123cbef0b").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Wrong 'password', status code 404: not found " + response.asString());
        response.then().statusCode(404); // 404

        //code is absent
        response = given().
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Parameter 'code' is absent, status code 401: unauthorized  " + response.asString());
        response.then().statusCode(401); // 401

        //email is absent
        response = given().
                param("code", code).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Parameter 'email' is absent, status code 400: bad request " + response.asString());
        response.then().statusCode(400); // 400

        // password is absent
        response = given().
                param("code", code).
                param("email", email).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Parameter 'password' is absent, status code 400:" + response.asString());
        response.then().statusCode(400); // 400

        // extra parameter
        response = given().
                param("code", code).
                param("parameter", "4235c0cbef0b").
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        logger.info("Extra parameter is added, status code 200:" + response.asString());
        response.then().statusCode(200); // 400? or 200

        sleep(5500);
    }
}
