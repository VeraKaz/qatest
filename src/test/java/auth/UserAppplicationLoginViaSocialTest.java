package auth;

import api.GetCode;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.Test;

import static api.GetCode.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class UserAppplicationLoginViaSocialTest {
        private static Logger logger = LogManager.getLogger();
        private int statusCode = 0;
        private static Response response = null;

    @Test
    public void testUserApplicationLoginViaSocial(){
        String social_code1 = getCode(Codes.WEB);
        response = given().
                param("social_uuid", "115577353747185978652").
                param("social", "google").
                param("code", social_code1).
                param("token", "ya29.GlvaBORMs1RBkPXztW5UtoCfCMf31x9ZMq6bqkSnIZmVPc8Xly2WcY6RQnjLIjwbITqmT-CxlVVw3Ma2xuIwhgPqg-AgKgKNG1AU1lRaNO_z4PfSnF8T1AbHxayN").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("Login Via Social, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(401); // 401 not authorized

        // correct social_uuid and token
        String social_uuid = "118404179903322899929";
        String social_token = "ya29.GlvwBPa4G8b-3J4yHH_jOL1_fvoJl07pIN7T6LrX_QjqOG5J11u0eT7WG05rlcoNIxly4GvE7SSIkColL-y8GTJucknPybWXr5mo1nV30llnjDg852ZKOURfGZiD";
        String social_code = getCode(Codes.WEB);

        response = given().
                param("social_uuid", social_uuid).
                param("social", "google").
                param("code", social_code).
                param("token", social_token).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("Login via social, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(404); // 401 not authorized
    }
}
