package auth;

import io.restassured.response.Response;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static api.GetCode.*;

@RunWith(JUnitParamsRunner.class)
public class ApplicationAuthorizeTest {
    private static Logger logger = LogManager.getLogger();
    private int statusCode = 0;
    private static Response response = null;

    @Parameters(value = {"android1", "ios1", "ftfs5iuio74dpiucujd8G6f6FIffuuffrkkigf"})
    @Test
    public void testApplicationAuthorize(String clientSecret) {
        //  int statusCode = 0;
        String client_id = "iwiefduxelh";
        String[] client_ids = {"iwiefduxelh", "wrong_client_id"};
        for (String client_id_test : client_ids) {
            response = given().
                    param("client_id", client_id_test).
                    param("client_secret", clientSecret).
                    param("response_type", "code").
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/authorize")
                    .then().
                            contentType(JSON)
                    .extract().
                            response();

            statusCode = response.path("status");
        if(client_id_test.equals("wrong_client_id")) {
            response.then().statusCode(400);
            } else {response.then().statusCode(200);}

            logger.info("Client_id and client_secret tests passed, client_id= " + client_id_test + ", client_secret= = " + clientSecret + ", StatusCode = " + statusCode + " JSON:" + response.asString());
        }

        // client_id is missed
        response = given().
                param("client_secret", clientSecret).
                param("response_type", "code").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("Parameter client_id is missed, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(400); // 400

        // client_secret is missing
        response = given().
                param("client_id", client_id).
                param("response_type", "code").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("Parameter client_secret is missed, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(400); // 400

        //response_type is missing
        response = given().
                param("client_id", client_id).
                param("client_secret", clientSecret).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("Parameter response_type is missing, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(401);

        sleep(3500);
    }
}
