package auth;
import io.restassured.response.Response;
import junitparams.JUnitParamsRunner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

import static api.GetCode.*;

@RunWith(JUnitParamsRunner.class)
public class UserLogoutTest {
         private static Logger logger = LogManager.getLogger();
         private static Response response = null;
         private int statusCode = 0;

        @Test
        public void testUserLogout() {
            String code_user_web = getCode(Codes.WEB);
            String code_user_ios = getCode(Codes.IOS);
            // get uuid and WEB token for test
            response = given().
                    param("code", code_user_web).
                    param("email", email).
                    param("password", password).
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/authorize")
                    .then().
                            contentType(JSON)
                    .extract().
                            response();

            String user_uuid = response.path("data.uuid");
            String user_token_web = response.path("data.token");

            // get IOS token for test
            response = given().
                    param("code", code_user_ios).
                    param("email", email).
                    param("password", password).
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/authorize")
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            String user_token_ios = response.path("data.token");

            // user logout
            response = given().
                    log().ifValidationFails().
                    when().
                    get("https://auth.keller-sports.com/oauth/v1/user/logout/" + user_uuid)
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            statusCode = response.path("status");
            response.then().statusCode(200); // 200
            logger.info("User logout test, status code:" + statusCode + ", JSON:" + response.asString());

            // try to logout with wrong user_uuid
            String wrong_user_uuid = "1287646546";
            response = given().
                    log().ifValidationFails().
                    when().
                    get("https://auth.keller-sports.com/oauth/v1/user/logout/" + wrong_user_uuid)
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            statusCode = response.path("status");
            response.then().statusCode(404); //
            logger.info("User logout test with wrong uuid status code:" + statusCode + ", JSON:" + response.asString());


            //get WEB token info after user logout
            response = given().
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/tokeninfo/" + user_uuid + "/" + user_token_web)
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            response.then().statusCode(401); // 401 not 404?
            statusCode = response.path("status");
            logger.info("WEB token info after logout, status code:" + statusCode + ", JSON:" + response.asString());

            //get IOS token info after user logout
            response = given().
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/tokeninfo/" + user_uuid + "/" + user_token_ios)
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            response.then().statusCode(401); // 401
            statusCode = response.path("status");
            logger.info("IOS token info after logout, status code:" + statusCode + ", JSON:" + response.asString());
        }
    }
