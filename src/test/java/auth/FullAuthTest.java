package auth;

import io.restassured.response.Response;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static api.GetCode.*;


@RunWith(JUnitParamsRunner.class)
public class FullAuthTest {
    private static int expectedStatusCode = 200;
    private static Response response = null;
    private static Logger logger = LogManager.getLogger();

    @Parameters(value = {"android1", "ios1", "ftfs5iuio74dpiucujd8G6f6FIffuuffrkkigf"})
    @Test
    public void testsAuthorize(String client_secret){

        // Get new code for WEB application, user: keller-sports
        response = given().
                param("client_id", "iwiefduxelh").
                param("client_secret", client_secret).
                param("response_type", "code").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().contentType(JSON)
                .extract().response();
        String new_code = response.path("data.code");
        //String json = response.asString();

        logger.info("json getCode:");
        response.body().prettyPrint();

        //Existed user authorisation with the received code
        response = given().
                param("code", new_code).
                param("email", email).
                param("password", password).
                param("scope", "basic").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        String uuid = response.path("data.uuid");
        String token = response.path("data.token");
        response.then().statusCode(expectedStatusCode);
        logger.info("json authorize");
        response.body().prettyPrint();

        // authorize uuid
        response = given().
                param("code", new_code).
                param("uuid", uuid).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize/uuid")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        response.then().statusCode(expectedStatusCode);
        logger.info("json authorize uuid");
        response.body().prettyPrint();

        // activate uuid
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/activate/uuid/" + uuid)
                .then().
                        contentType(JSON)
                .extract().
                        response();
        response.then().statusCode(expectedStatusCode);
        logger.info("json activate uuid");
        response.body().prettyPrint();

        //getTokenInfo
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/tokeninfo/"+ uuid +"/" + token)
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json getTokenInfo");
        response.body().prettyPrint();
        response.then().statusCode(expectedStatusCode);

        //testGetCodeInfo
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/codeinfo/"+ new_code)
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json getcodeInfo");
        response.body().prettyPrint();
        response.then().statusCode(expectedStatusCode);

        //application logout
        response = given().
                param("uuid", uuid).
                param("token", token).
                log().ifValidationFails().
                when().
                get("https://auth.keller-sports.com/oauth/v1/logout")
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json applicationLogout");
        response.body().prettyPrint();
        response.then().statusCode(expectedStatusCode);

        //get token info after applicationlogout
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/tokeninfo/"+ uuid +"/" + token)
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json getTokenInfo after application logout");
        response.body().prettyPrint();
        response.then().statusCode(401);

        //user logout
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/user/logout/" + uuid)
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json user logout ");
        response.body().prettyPrint();
        response.then().statusCode(expectedStatusCode);

        //get token info after userLogout
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/tokeninfo/"+ uuid +"/" + token)
                .then().
                        contentType(JSON)
                .extract().
                        response();

        logger.info("json getTokenInfo after user logout");
        response.body().prettyPrint();
        response.then().statusCode(401);

         // Application login via social
            response = given().
                    param("social_uuid", "115577353747185978652").
                    param("social", "google").
                    param("code", new_code).
                    param("token", token).
                    log().ifValidationFails().
                    when().
                    get("https://auth.keller-sports.com/oauth/v1/authorize")
                    .then().
                            contentType(JSON)
                    .extract().
                            response();

            logger.info("json applicationLogout");
            response.body().prettyPrint();
            response.then().statusCode(404); // expected 200

        sleep(5500);
    }

}
