package auth;

import io.restassured.response.Response;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import static api.GetCode.*;
import static api.GetCode.email;
import static api.GetCode.password;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class UserApplicationLogoutTest {
    private static Logger logger = LogManager.getLogger();
    private int statusCode = 0;
    private static Response response = null;

    @Parameters(value = {"android1", "ios1", "ftfs5iuio74dpiucujd8G6f6FIffuuffrkkigf"})
    @Test
    public void testUserApplicationLogout(String clientSecret) {
        // get code for different application
        response = given().
                param("client_id", "iwiefduxelh").
                param("client_secret", clientSecret).
                param("response_type", "code").
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        new_code = response.path("data.code");

        // get uuid and token for test
        response = given().
                param("code", new_code).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        String uuid_user = response.path("data.uuid");
        String token_user = response.path("data.token");

        //get WEB code for test logout
        String new_codeWeb = getCode(Codes.WEB);

        //get WEB token for test logout
        response = given().
                param("code", new_codeWeb).
                param("email", email).
                param("password", password).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/authorize")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        String token_userWeb = response.path("data.token");

        // correct logout
        response = given().
                param("uuid", uuid_user).
                param("token", token_user).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/logout")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        logger.info("User application " + clientSecret + " logout test, status code:" + statusCode + ", JSON:" + response.asString());
        response.then().statusCode(200);

        // check application token info after logout
        response = given().
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/tokeninfo/" + uuid_user + "/" + token_user)
                .then().
                        contentType(JSON)
                .extract().
                        response();
        response.then().statusCode(401); // 401 not 404?
        statusCode = response.path("status");
        logger.info("Application token info after application logout, status code:" + statusCode + ", JSON:" + response.asString());

        // check WEB token info after application logout
        if(!token_user.equals(token_userWeb)){
            response = given().
                    log().ifValidationFails().
                    when().
                    get("http://auth.keller-sports.com/oauth/v1/tokeninfo/" + uuid_user + "/" + token_userWeb)
                    .then().
                            contentType(JSON)
                    .extract().
                            response();
            response.then().statusCode(200);
            statusCode = response.path("status");
            logger.info("WEB token info after application logout " + clientSecret + ", status code:" + statusCode + ", JSON:" + response.asString());
        }

        sleep(4500);
        // try to logout second time
        response = given().
                param("uuid", uuid_user).
                param("token", token_user).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/logout")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        String message = response.path("message");
        response.then().statusCode(401); // 401 not authorized
        assertEquals("The selected token is invalid.", message );
        logger.info("User application second logout test, status code:" + statusCode + ", JSON:" + response.asString());

        //uuid is missing
        response = given().
                param("token", token_user).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/logout")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");
        response.then().statusCode(404); // 404 not found? or 401 bad request
        logger.info("Uuid is missing, status code:" + statusCode + ", JSON:" + response.asString());

        // token is missing
        response = given().
                param("uuid", uuid_user).
                log().ifValidationFails().
                when().
                get("http://auth.keller-sports.com/oauth/v1/logout")
                .then().
                        contentType(JSON)
                .extract().
                        response();
        statusCode = response.path("status");

        response.then().statusCode(401); // 404 not found? or 400 bad request
        assertEquals("The selected token is invalid.", message );
        logger.info("Token is missing, status code:" + statusCode + ", JSON:" + response.asString());

        sleep(8500);
    }
}
