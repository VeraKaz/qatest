package auth;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(value = {UserApplicationLogoutTest.class, ApplicationAuthorizeTest.class,
        FullAuthTest.class,
        UserLogoutTest.class,
        UserApplicationLoginTest.class,

        UserAppplicationLoginViaSocialTest.class
        })
public class TestSuite {
}

