package api;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetCode {
       // public static Logger logger = LogManager.getLogger();
        public enum Codes {
            WEB, IOS, ANDOID
        }

        private static Response response = null;
        public static String new_code;
        public static String email = "vera.kazimirova@uadevelopers.com";
        public static String password = "46c5c0cbef0b";

        public static String getCode(Codes codes) {

            switch (codes) {
                case IOS: {
                    response = given().
                            param("client_id", "iwiefduxelh").
                            param("client_secret", "ios1").
                            param("response_type", "code").
                            log().ifValidationFails().
                            when().
                            get("http://auth.keller-sports.com/oauth/v1/authorize")
                            .then().
                                    contentType(JSON)
                            .extract().
                                    response();
                    new_code = response.path("data.code");
                    return new_code;
                }
                //  break;

                case ANDOID: {
                    response = given().
                            param("client_id", "iwiefduxelh").
                            param("client_secret", "android1").
                            param("response_type", "code").
                            log().ifValidationFails().
                            when().
                            get("http://auth.keller-sports.com/oauth/v1/authorize")
                            .then().
                                    contentType(JSON)
                            .extract().
                                    response();
                    new_code = response.path("data.code");
                    return new_code;

                }
                //    break;

                case WEB: {
                    response = given().
                            param("client_id", "iwiefduxelh").
                            param("client_secret", "ftfs5iuio74dpiucujd8G6f6FIffuuffrkkigf").
                            param("response_type", "code").
                            log().ifValidationFails().
                            when().
                            get("http://auth.keller-sports.com/oauth/v1/authorize")
                            .then().
                                    contentType(JSON)
                            .extract().
                                    response();
                    new_code = response.path("data.code");
                    return new_code;
                }
                //     break;
            }
            return new_code;
        }

    public static void sleep(int mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
